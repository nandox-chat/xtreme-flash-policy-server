# -----------------------------------------------------------------
# Note 1: You must configure this file and rename it to 'config.pm'
# Note 2: Lines beginning with # will be ignored
# Note 3: Don't edit system stuff or program may not work correctly!
# -----------------------------------------------------------------

package config;

# ------------------------------------
# Config Start for Xtreme Flash Policy
# ------------------------------------

our %conf = (

# --------------------------
# Public policy file serving
# --------------------------
    'POLICY_BINDADDR'       => '0.0.0.0',                           # Bind IP address (Normally 0.0.0.0 to listen on all inet interfases)
    'POLICY_PORT'           => 843,                                 # Listen port (default is 843 and requires root privileges or aditional platform OS customization)
    'POLICY_PORT_FALLBACK'  => 8002,                                # Fallback listen port (default is 8002 but you may choose another above 1024 for unprivileged user account)
    'POLICY_CONNTIMEOUT'    => 3,                                   # Timeout for clients connections (avoids connection long time inactivity)
    'POLICY_CONNQUEUE'      => 5,                                   # How many connections we want to enqueue. You may want to increanse this value if next option is 0=QUEUE
    'POLICY_PEERPOOLMETHOD' => 0,                                   # Peer connection method to use: 0=QUEUE, 1=FORK, 2=THREAD
    'POLICY_ALLOW_HOST'     => '*',                                 # Host to allow on reply (another value rather than '*' is rarely used)
    'POLICY_ALLOW_PORT'     => '6667,6697,7000-8000',               # Port/s to allow on reply (separated by comma, also you can specify port ranges by using port-port)

# -----------
# Log to file
# -----------
    'POLICY_LOG'            => 1,                                   # Enable/disable file logging
    'POLICY_LOGFILE'        => '/tmp/lookflashpolicy.log',          # File to log events (/var/log/lookflashpolicy.log is a good place, verify ownership and writte permissions)

# ---------------------------
# Connection flood protection
# ---------------------------
    'FLOODPRO_ENABLE'       => 1,                                   # Enable/disable connection flood protection per IP addresses
    'FLOODPRO_CONNMAX'      => 20,                                  # Maximun connections per IP address in FLOODPRO_CONNTIME threshold. If exceeds address will be blacklisted.
    'FLOODPRO_CONNTIME'     => 1,                                   # Time in minutes where FLOODPRO_CONNMAX amount of connections per address allowed threshold. If exceeds address will be blacklisted.
    'FLOODPRO_ADDREXPIRES'  => 10,                                  # Time in minutes to expires blacklisted address (use zero for non expiration time)
    'FLOODPRO_TRIGGER_CMD'  => '',                                  # FOR ADVANCED USERS ONLY: You can set a command to be executed when IP address is added to blacklist
                                                                    # Some ideas:
                                                                    #   * Add an iptables rule to block IP at network layer 3 'iptables -A INPUT -s <IP_ADDRESS> -j DROP'
                                                                    #   * Add IP address in a BSD's Packet Filter table '/sbin/pfctl -t flooders -Tadd <IP_ADDRESS>'
                                                                    #   * Call a custom script '/usr/local/bin/notify_attack.pl <IP_ADDRESS> <IP_EXPIRE>'
                                                                    # You can use <IP_ADDRESS> (IP address added in blacklist) and <IP_EXPIRE> (Expiration time in minutes) tags as parameter variables.
                                                                    # Leave this config empty '' to disable.
                                                                    # If you run lookflashpolicy under non root user you must to add this line to your /etc/sudoers (supposing lookflash is your user)
                                                                    # For Linux:
                                                                    # lookflash ALL=(ALL) NOPASSWD: /sbin/iptables
                                                                    # For BSD:
                                                                    # lookflash ALL=(ALL) NOPASSWD: /sbin/pfctl

# -------------------
# Log to IRC services
# -------------------
    'IRC_ENABLE'            => 1,                                   # Enable/disable IRC Feature
    'IRC_SERVER'            => '127.0.0.1',                         # IRC Server host
    'IRC_PORT'              => 6667,                                # IRC Server port
    'IRC_RECONNECTSECS'     => 3,                                   # Seconds to wait for reconnection if connection lost
    'IRC_NICK'              => 'PolicyServ',                        # Nickname
    'IRC_REALNAME'          => 'NandOX Xtreme Flash Policy',        # Realname
    'IRC_USERNAME'          => 'Xtreme',                            # Username
    'IRC_NICKSERV'          => 'privmsg nickserv :identify SECRET', # Command to identify with NickServ
    'IRC_OPER'              => 'PolicyServ SECRET',                 # Command to identify as oper (It's recommended to give oper access to prevent bot disconnection by server due to flood)
    'IRC_MODE'              => '-h',                                # User modes (ie. -h prevent helper)
    'IRC_AWAY'              => 'Endurance...',                      # Away message
    'IRC_CHANNEL'           => '#services',                         # Channel to join, output log and trigger !fantasy commands (HIGHLY recommended this channel to be +O IRCops only!)
    'IRC_FANTASY'           => 1,                                   # Enable/disable use of fantasy commands (!policy, !policy alive, !policy stats)
    'IRC_FANTASY_PREFIX'    => "!policy",                           # !fantasy prefix trigger
    'IRC_LOG'               => 1,                                   # Enable/disable channel log to monitor activity in real-time by IRCops

# ----------------
# General settings
# ----------------
    'LOOK_PIDFILE'          => '/tmp/lookflashpolicy.pid',          # Pid file (/home/user/lookflashpolicy.pid may be a good place for this)
    'LOOK_ROOTNOTICE'       => 1,                                   # Check and show security risk notice if running as root
    'UPTIME_CMD'            => '/usr/bin/uptime',                   # Fullpath to retrieve uptime and load average
    'FREE_CMD'              => '/usr/bin/free',                     # Fullpath to retrieve system memory usage
);

# ----------------------------------
# Config end for Xtreme Flash Policy
# ----------------------------------

# -------------------------------------
# Needed system stuff. Don't edit below
# -------------------------------------

use strict;
use warnings;

require Exporter;

our @ISA = qw(Exporter);
our @EXPORT_OK = qw(%conf);

1;
